# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=6.0
NUGETS="
benchmarkdotnet.annotations-0.13.2
benchmarkdotnet-0.13.2
commandlineparser-2.4.3
comparenetobjects-4.78.0
configureawaitchecker.analyzer-5.0.0.1
coverlet.collector-3.1.2
esprima-2.1.2
eventstore.client.grpc.streams-22.0.0
eventstore.client.grpc-22.0.0
eventstore.client-21.2.0
eventstore.plugins-22.10.1
githubactionstestlogger-2.0.1
gitinfo-2.0.26
google.protobuf-3.21.6
grpc.aspnetcore.server.clientfactory-2.49.0
grpc.aspnetcore.server-2.49.0
grpc.aspnetcore-2.49.0
grpc.core.api-2.46.5
grpc.core.api-2.49.0
grpc.core-2.46.5
grpc.net.client-2.49.0
grpc.net.clientfactory-2.49.0
grpc.net.common-2.49.0
grpc.tools-2.49.1
hdrhistogram-2.5.0
hoststat.net-1.0.2
iced-1.17.0
jint-3.0.0-beta-2038
microsoft.aspnetcore.app.runtime.linux-arm-6.0.12
microsoft.aspnetcore.app.runtime.linux-arm64-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-arm-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-arm64-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-x64-6.0.12
microsoft.aspnetcore.app.runtime.linux-x64-6.0.12
microsoft.aspnetcore.testhost-6.0.9
microsoft.bcl.asyncinterfaces-1.1.0
microsoft.bcl.asyncinterfaces-5.0.0
microsoft.bcl.asyncinterfaces-6.0.0
microsoft.codeanalysis.analyzers-2.6.2-beta2
microsoft.codeanalysis.common-3.0.0
microsoft.codeanalysis.csharp-3.0.0
microsoft.codecoverage-16.6.1
microsoft.codecoverage-17.3.2
microsoft.csharp-4.0.1
microsoft.csharp-4.4.1
microsoft.csharp-4.7.0
microsoft.data.sqlite.core-6.0.10
microsoft.data.sqlite-6.0.10
microsoft.diagnostics.netcore.client-0.2.251802
microsoft.diagnostics.netcore.client-0.2.328102
microsoft.diagnostics.runtime-2.2.332302
microsoft.diagnostics.tracing.traceevent-3.0.2
microsoft.diagnostics.tracing.traceevent-3.0.5
microsoft.dotnet.platformabstractions-3.1.6
microsoft.extensions.configuration.abstractions-2.0.0
microsoft.extensions.configuration.abstractions-2.1.1
microsoft.extensions.configuration.abstractions-3.0.3
microsoft.extensions.configuration.abstractions-6.0.0
microsoft.extensions.configuration.binder-2.0.0
microsoft.extensions.configuration.binder-2.1.1
microsoft.extensions.configuration.binder-3.0.3
microsoft.extensions.configuration.fileextensions-6.0.0
microsoft.extensions.configuration.json-6.0.0
microsoft.extensions.configuration-2.0.0
microsoft.extensions.configuration-2.1.1
microsoft.extensions.configuration-3.0.3
microsoft.extensions.configuration-6.0.0
microsoft.extensions.dependencyinjection.abstractions-2.0.0
microsoft.extensions.dependencyinjection.abstractions-2.1.1
microsoft.extensions.dependencyinjection.abstractions-3.0.3
microsoft.extensions.dependencyinjection.abstractions-5.0.0
microsoft.extensions.dependencyinjection-3.0.3
microsoft.extensions.dependencyinjection-5.0.0
microsoft.extensions.dependencymodel-3.0.0
microsoft.extensions.fileproviders.abstractions-6.0.0
microsoft.extensions.fileproviders.composite-6.0.0
microsoft.extensions.fileproviders.embedded-6.0.9
microsoft.extensions.fileproviders.physical-6.0.0
microsoft.extensions.filesystemglobbing-6.0.0
microsoft.extensions.http-3.0.3
microsoft.extensions.logging.abstractions-2.0.0
microsoft.extensions.logging.abstractions-2.1.1
microsoft.extensions.logging.abstractions-3.0.3
microsoft.extensions.logging.abstractions-5.0.0
microsoft.extensions.logging-2.0.0
microsoft.extensions.logging-2.1.1
microsoft.extensions.logging-3.0.3
microsoft.extensions.logging-5.0.0
microsoft.extensions.objectpool-5.0.10
microsoft.extensions.options-2.0.0
microsoft.extensions.options-2.1.1
microsoft.extensions.options-3.0.3
microsoft.extensions.options-5.0.0
microsoft.extensions.primitives-2.0.0
microsoft.extensions.primitives-2.1.1
microsoft.extensions.primitives-2.2.0
microsoft.extensions.primitives-3.0.3
microsoft.extensions.primitives-5.0.0
microsoft.extensions.primitives-6.0.0
microsoft.faster.core-1.9.5
microsoft.net.http.headers-2.2.8
microsoft.net.test.sdk-16.6.1
microsoft.net.test.sdk-17.3.2
microsoft.netcore.app.host.linux-arm-6.0.12
microsoft.netcore.app.host.linux-arm64-6.0.12
microsoft.netcore.app.host.linux-musl-arm-6.0.12
microsoft.netcore.app.host.linux-musl-arm64-6.0.12
microsoft.netcore.app.host.linux-musl-x64-6.0.12
microsoft.netcore.app.runtime.linux-arm-6.0.12
microsoft.netcore.app.runtime.linux-arm64-6.0.12
microsoft.netcore.app.runtime.linux-musl-arm-6.0.12
microsoft.netcore.app.runtime.linux-musl-arm64-6.0.12
microsoft.netcore.app.runtime.linux-musl-x64-6.0.12
microsoft.netcore.app.runtime.linux-x64-6.0.12
microsoft.netcore.platforms-1.0.1
microsoft.netcore.platforms-1.1.0
microsoft.netcore.platforms-1.1.1
microsoft.netcore.platforms-2.0.0
microsoft.netcore.platforms-2.1.0
microsoft.netcore.targets-1.0.1
microsoft.netcore.targets-1.1.0
microsoft.netcore.targets-1.1.3
microsoft.netframework.referenceassemblies.net461-1.0.0
microsoft.netframework.referenceassemblies-1.0.0
microsoft.testplatform.objectmodel-16.6.1
microsoft.testplatform.objectmodel-17.2.0
microsoft.testplatform.objectmodel-17.3.2
microsoft.testplatform.testhost-16.6.1
microsoft.testplatform.testhost-17.3.2
microsoft.win32.primitives-4.3.0
microsoft.win32.systemevents-6.0.0
minver-4.2.0
mono.posix.netstandard-1.0.0
netstandard.library-1.6.1
netstandard.library-2.0.0
netstandard.library-2.0.3
newtonsoft.json-11.0.2
newtonsoft.json-13.0.2
newtonsoft.json-9.0.1
nuget.frameworks-5.0.0
nuget.frameworks-5.11.0
nunit-3.13.3
nunit3testadapter-4.2.1
objectlayoutinspector-0.1.4
perfolizer-0.2.1
protobuf-net-2.4.0
runtime.any.system.collections-4.3.0
runtime.any.system.diagnostics.tools-4.3.0
runtime.any.system.diagnostics.tracing-4.3.0
runtime.any.system.globalization.calendars-4.3.0
runtime.any.system.globalization-4.3.0
runtime.any.system.io-4.3.0
runtime.any.system.reflection.extensions-4.3.0
runtime.any.system.reflection.primitives-4.3.0
runtime.any.system.reflection-4.3.0
runtime.any.system.resources.resourcemanager-4.3.0
runtime.any.system.runtime.handles-4.3.0
runtime.any.system.runtime.interopservices-4.3.0
runtime.any.system.runtime-4.3.0
runtime.any.system.text.encoding.extensions-4.3.0
runtime.any.system.text.encoding-4.3.0
runtime.any.system.threading.tasks-4.3.0
runtime.any.system.threading.timer-4.3.0
runtime.debian.8-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.debian.8-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.fedora.23-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.23-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.fedora.24-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.24-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.native.system.io.compression-4.3.0
runtime.native.system.net.http-4.3.0
runtime.native.system.security.cryptography.apple-4.3.0
runtime.native.system.security.cryptography.openssl-4.3.0
runtime.native.system.security.cryptography.openssl-4.3.2
runtime.native.system-4.3.0
runtime.opensuse.13.2-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.opensuse.13.2-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.opensuse.42.1-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.opensuse.42.1-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.apple-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.rhel.7-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.rhel.7-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.ubuntu.14.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.14.04-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.ubuntu.16.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.04-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.ubuntu.16.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.10-x64.runtime.native.system.security.cryptography.openssl-4.3.2
runtime.unix.microsoft.win32.primitives-4.3.0
runtime.unix.system.console-4.3.0
runtime.unix.system.console-4.3.1
runtime.unix.system.diagnostics.debug-4.3.0
runtime.unix.system.io.filesystem-4.3.0
runtime.unix.system.net.primitives-4.3.0
runtime.unix.system.net.sockets-4.3.0
runtime.unix.system.private.uri-4.3.0
runtime.unix.system.runtime.extensions-4.3.0
serilog.enrichers.process-2.0.2
serilog.enrichers.thread-3.1.0
serilog.expressions-3.4.0
serilog.extensions.logging-3.1.0
serilog.filters.expressions-2.1.0
serilog.settings.configuration-3.4.0
serilog.sinks.async-1.5.0
serilog.sinks.console-4.1.0
serilog.sinks.file-5.0.0
serilog.sinks.textwriter-2.1.0
serilog-2.0.0
serilog-2.12.0
serilog-2.9.0
sqlitepclraw.bundle_e_sqlite3-2.1.2
sqlitepclraw.core-2.1.2
sqlitepclraw.lib.e_sqlite3-2.1.2
sqlitepclraw.provider.e_sqlite3-2.1.2
superpower-2.3.0
system.appcontext-4.3.0
system.buffers-4.3.0
system.buffers-4.5.0
system.codedom-6.0.0
system.collections.concurrent-4.3.0
system.collections.immutable-1.5.0
system.collections.immutable-5.0.0
system.collections-4.0.11
system.collections-4.3.0
system.commandline.dragonfruit-0.3.0-alpha.20574.7
system.commandline.rendering-0.3.0-alpha.20574.7
system.commandline-2.0.0-beta1.20574.7
system.componentmodel.composition-6.0.0
system.configuration.configurationmanager-6.0.0
system.console-4.3.0
system.diagnostics.debug-4.0.11
system.diagnostics.debug-4.3.0
system.diagnostics.diagnosticsource-4.3.0
system.diagnostics.performancecounter-6.0.1
system.diagnostics.tools-4.0.1
system.diagnostics.tools-4.3.0
system.diagnostics.tracing-4.3.0
system.drawing.common-6.0.0
system.dynamic.runtime-4.0.11
system.formats.asn1-6.0.0
system.globalization.calendars-4.3.0
system.globalization.extensions-4.3.0
system.globalization-4.0.11
system.globalization-4.3.0
system.interactive.async-5.0.0
system.io.compression.zipfile-4.3.0
system.io.compression-4.3.0
system.io.filesystem.primitives-4.0.1
system.io.filesystem.primitives-4.3.0
system.io.filesystem-4.0.1
system.io.filesystem-4.3.0
system.io.pipelines-6.0.3
system.io-4.1.0
system.io-4.3.0
system.linq.async-5.0.0
system.linq.async-6.0.1
system.linq.expressions-4.1.0
system.linq.expressions-4.3.0
system.linq-4.1.0
system.linq-4.3.0
system.management-6.0.0
system.memory-4.5.1
system.memory-4.5.3
system.memory-4.5.4
system.net.http-4.3.0
system.net.http-4.3.4
system.net.nameresolution-4.3.0
system.net.primitives-4.3.0
system.net.sockets-4.3.0
system.numerics.vectors-4.5.0
system.objectmodel-4.0.12
system.objectmodel-4.3.0
system.private.servicemodel-4.10.0
system.private.servicemodel-4.5.3
system.private.uri-4.3.0
system.reflection.dispatchproxy-4.5.0
system.reflection.dispatchproxy-4.7.1
system.reflection.emit.ilgeneration-4.0.1
system.reflection.emit.ilgeneration-4.3.0
system.reflection.emit.lightweight-4.0.1
system.reflection.emit.lightweight-4.3.0
system.reflection.emit.lightweight-4.7.0
system.reflection.emit-4.0.1
system.reflection.emit-4.3.0
system.reflection.emit-4.7.0
system.reflection.extensions-4.0.1
system.reflection.extensions-4.3.0
system.reflection.metadata-1.6.0
system.reflection.primitives-4.0.1
system.reflection.primitives-4.3.0
system.reflection.typeextensions-4.1.0
system.reflection.typeextensions-4.3.0
system.reflection-4.1.0
system.reflection-4.3.0
system.resources.resourcemanager-4.0.1
system.resources.resourcemanager-4.3.0
system.runtime.compilerservices.unsafe-4.4.0
system.runtime.compilerservices.unsafe-4.5.0
system.runtime.compilerservices.unsafe-4.5.1
system.runtime.compilerservices.unsafe-4.5.3
system.runtime.compilerservices.unsafe-5.0.0
system.runtime.compilerservices.unsafe-6.0.0
system.runtime.extensions-4.1.0
system.runtime.extensions-4.3.0
system.runtime.handles-4.0.1
system.runtime.handles-4.3.0
system.runtime.interopservices.runtimeinformation-4.3.0
system.runtime.interopservices-4.1.0
system.runtime.interopservices-4.3.0
system.runtime.numerics-4.3.0
system.runtime.serialization.primitives-4.1.1
system.runtime-4.1.0
system.runtime-4.3.0
system.runtime-4.3.1
system.security.accesscontrol-6.0.0
system.security.claims-4.3.0
system.security.cryptography.algorithms-4.3.0
system.security.cryptography.cng-4.3.0
system.security.cryptography.csp-4.3.0
system.security.cryptography.encoding-4.3.0
system.security.cryptography.openssl-4.3.0
system.security.cryptography.pkcs-6.0.1
system.security.cryptography.primitives-4.3.0
system.security.cryptography.protecteddata-6.0.0
system.security.cryptography.x509certificates-4.3.0
system.security.cryptography.xml-6.0.1
system.security.permissions-6.0.0
system.security.principal.windows-4.3.0
system.security.principal.windows-4.5.0
system.security.principal.windows-5.0.0
system.security.principal-4.3.0
system.servicemodel.http-4.10.0
system.servicemodel.primitives-4.10.0
system.servicemodel.primitives-4.5.3
system.text.encoding.codepages-4.5.0
system.text.encoding.extensions-4.0.11
system.text.encoding.extensions-4.3.0
system.text.encoding-4.0.11
system.text.encoding-4.3.0
system.text.encodings.web-6.0.0
system.text.json-4.6.0
system.text.json-6.0.0
system.text.regularexpressions-4.1.0
system.text.regularexpressions-4.3.0
system.text.regularexpressions-4.3.1
system.threading.tasks.extensions-4.0.0
system.threading.tasks.extensions-4.3.0
system.threading.tasks.extensions-4.5.4
system.threading.tasks-4.0.11
system.threading.tasks-4.3.0
system.threading.threadpool-4.3.0
system.threading.timer-4.3.0
system.threading-4.0.11
system.threading-4.3.0
system.windows.extensions-6.0.0
system.xml.readerwriter-4.0.11
system.xml.readerwriter-4.3.0
system.xml.xdocument-4.0.11
system.xml.xdocument-4.3.0
xunit.abstractions-2.0.3
xunit.analyzers-1.0.0
xunit.assert-2.4.2
xunit.core-2.4.2
xunit.extensibility.core-2.4.2
xunit.extensibility.execution-2.4.2
xunit.runner.visualstudio-2.4.5
xunit-2.4.2
yamldotnet-12.0.1
"

inherit check-reqs dotnet-pkg

DESCRIPTION="Functional database with Complex Event Processing in JavaScript"
HOMEPAGE="https://eventstore.com/
	https://github.com/EventStore/EventStore/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/EventStore/EventStore.git"
else
	SRC_URI="
		https://github.com/EventStore/EventStore/archive/oss-v${PV}.tar.gz
			-> ${P}.tar.gz
		https://github.com/EventStore/EventStore.UI/archive/oss-v${PV}.tar.gz
			-> ${PN}-ui-${PV}.tar.gz
	"
	S="${WORKDIR}"/EventStore-oss-v${PV}
	KEYWORDS="~amd64"
fi

SRC_URI+=" $(nuget_uris) "

LICENSE="BSD"
SLOT="0"

CHECKREQS_DISK_BUILD="2G"
DOTNET_PROJECTS=(
	"${S}/src/EventStore.ClusterNode/EventStore.ClusterNode.csproj"
)

DOCS=( CHANGELOG.md CONTRIBUTING.md README.md SECURITY.md )

pkg_setup() {
	check-reqs_pkg_setup
	dotnet-pkg_pkg_setup
}

src_unpack() {
	dotnet-pkg_src_unpack

	[[ ${EGIT_REPO_URI} ]] && git-r3_src_unpack
}

src_prepare() {
	dotnet-pkg_src_prepare

	if [[ ${PV} == *9999* ]] ; then
		cp -r "${WORKDIR}"/EventStore.UI-oss-v${PV}/. \
		   "${S}"/src/EventStore.UI/ || die
	else
		eapply "${FILESDIR}"/${PN}-22.10.1-version.patch

		sed -i "s|@PV@|${PV}.0|" src/Directory.Build.props || die
	fi
}

src_install() {
	dotnet-pkg-utils_install
	dotnet-pkg-utils_dolauncher /usr/share/${P}/EventStore.ClusterNode
	dosym -r /usr/bin/EventStore.ClusterNode /usr/bin/eventstored

	insinto /etc/${PN}
	doins "${FILESDIR}"/${PN}.conf

	keepdir /var/{lib,log}/${PN}

	einstalldocs
}
