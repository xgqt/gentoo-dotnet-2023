# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_PN=NickvisionMoney

DOTNET_COMPAT=7.0
PYTHON_COMPAT=( python3_{9..11} )

NUGETS="
docnet.core-2.3.1
gircore.adw-1-0.3.0
gircore.cairo-1.0-0.3.0
gircore.freetype2-2.0-0.3.0
gircore.gdk-4.0-0.3.0
gircore.gdkpixbuf-2.0-0.3.0
gircore.gio-2.0-0.3.0
gircore.glib-2.0-0.3.0
gircore.gobject-2.0-0.3.0
gircore.graphene-1.0-0.3.0
gircore.gsk-4.0-0.3.0
gircore.gtk-4.0-0.3.0
gircore.harfbuzz-0.0-0.3.0
gircore.pango-1.0-0.3.0
harfbuzzsharp.nativeassets.linux-2.8.2.3
harfbuzzsharp.nativeassets.macos-2.8.2.3
harfbuzzsharp.nativeassets.win32-2.8.2.3
harfbuzzsharp-2.8.2.3
hazzik.qif-1.0.3
microsoft.data.sqlite.core-7.0.3
microsoft.data.sqlite.core-7.0.4
microsoft.netcore.platforms-1.1.0
microsoft.netcore.platforms-5.0.0
microsoft.netcore.targets-5.0.0
microsoft.win32.primitives-4.3.0
netstandard.library-1.6.1
ofxsharp.netstandard-1.0.0
pdfsharpcore-1.3.49
questpdf-2022.12.2
readsharp.ports.sgmlreader.core-1.0.0
runtime.debian.8-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.23-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.24-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.native.system.io.compression-4.3.0
runtime.native.system.net.http-4.3.0
runtime.native.system.security.cryptography.apple-4.3.0
runtime.native.system.security.cryptography.openssl-4.3.0
runtime.native.system-4.3.0
runtime.opensuse.13.2-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.opensuse.42.1-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.apple-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.rhel.7-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.14.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
sharpziplib-1.3.3
sixlabors.fonts-1.0.0-beta17
sixlabors.imagesharp-2.1.3
skiasharp.harfbuzz-2.88.3
skiasharp.nativeassets.linux-2.88.3
skiasharp.nativeassets.macos-2.88.3
skiasharp.nativeassets.win32-2.88.3
skiasharp-2.88.3
sqlitepclraw.bundle_e_sqlcipher-2.1.4
sqlitepclraw.core-2.1.4
sqlitepclraw.lib.e_sqlcipher-2.1.4
sqlitepclraw.provider.e_sqlcipher-2.1.4
system.appcontext-4.3.0
system.buffers-4.3.0
system.collections.concurrent-4.3.0
system.collections-4.3.0
system.console-4.3.0
system.diagnostics.debug-4.3.0
system.diagnostics.diagnosticsource-4.3.0
system.diagnostics.tools-4.3.0
system.diagnostics.tracing-4.3.0
system.globalization.calendars-4.3.0
system.globalization.extensions-4.3.0
system.globalization-4.3.0
system.io.compression.zipfile-4.3.0
system.io.compression-4.3.0
system.io.filesystem.primitives-4.3.0
system.io.filesystem-4.3.0
system.io-4.3.0
system.linq.expressions-4.3.0
system.linq-4.3.0
system.memory-4.5.3
system.net.http-4.3.0
system.net.primitives-4.3.0
system.net.requests-4.3.0
system.net.sockets-4.3.0
system.net.webheadercollection-4.3.0
system.objectmodel-4.3.0
system.reflection.emit.ilgeneration-4.3.0
system.reflection.emit.lightweight-4.3.0
system.reflection.emit-4.3.0
system.reflection.extensions-4.3.0
system.reflection.primitives-4.3.0
system.reflection.typeextensions-4.3.0
system.reflection-4.3.0
system.resources.resourcemanager-4.3.0
system.runtime.compilerservices.unsafe-5.0.0
system.runtime.extensions-4.3.0
system.runtime.handles-4.3.0
system.runtime.interopservices.runtimeinformation-4.3.0
system.runtime.interopservices-4.3.0
system.runtime.numerics-4.3.0
system.runtime-4.3.0
system.security.cryptography.algorithms-4.3.0
system.security.cryptography.cng-4.3.0
system.security.cryptography.csp-4.3.0
system.security.cryptography.encoding-4.3.0
system.security.cryptography.openssl-4.3.0
system.security.cryptography.primitives-4.3.0
system.security.cryptography.x509certificates-4.3.0
system.text.encoding.codepages-5.0.0
system.text.encoding.extensions-4.3.0
system.text.encoding-4.3.0
system.text.regularexpressions-4.3.0
system.threading.tasks.extensions-4.3.0
system.threading.tasks-4.3.0
system.threading.timer-4.3.0
system.threading-4.3.0
system.xml.readerwriter-4.3.0
system.xml.xdocument-4.3.0
"

inherit check-reqs desktop dotnet-pkg gnome2-utils python-any-r1 xdg

DESCRIPTION="A personal finance manager"
HOMEPAGE="https://github.com/nlogozzo/NickvisionMoney/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/nlogozzo/${MY_PN}.git"
	S="${S}"/${MY_PN}.GNOME
else
	SRC_URI="https://github.com/nlogozzo/${MY_PN}/archive/${PV}.tar.gz
		-> ${P}.tar.gz"
	S="${WORKDIR}"/${P^}/${MY_PN}.GNOME
	KEYWORDS="~amd64"
fi

SRC_URI+=" $(nuget_uris) "

LICENSE="MIT"
SLOT="0"

RDEPEND="
	>=gui-libs/gtk-4.10:4
	app-arch/brotli
	dev-db/sqlite:3
	dev-libs/glib
	gui-libs/libadwaita:1
	media-libs/freetype
	media-libs/harfbuzz
"
BDEPEND="
	${PYTHON_DEPS}
	${RDEPEND}
	dev-util/blueprint-compiler
"

CHECKREQS_DISK_BUILD="1G"
DOTNET_PROJECTS=( "${S}/NickvisionMoney.GNOME.csproj" )

pkg_setup() {
	check-reqs_pkg_setup
	dotnet-pkg_pkg_setup
	python-any-r1_pkg_setup
}

src_unpack() {
	dotnet-pkg_src_unpack

	[[ ${EGIT_REPO_URI} ]] && git-r3_src_unpack
}

src_compile() {
	ebegin "Compiling blueprints"
	blueprint-compiler batch-compile Blueprints/ Blueprints/ Blueprints/*.blp
	eend ${?} || die "failed to compile blueprints"

	ebegin "Compiling gresources"
	glib-compile-resources \
		--sourcedir Resources Resources/org.nickvision.money.gresource.xml
	eend ${?} || die "failed to compile gresources"

	dotnet-pkg_src_compile
}

src_install() {
	dotnet-pkg-utils_install
	dotnet-pkg-utils_dolauncher /usr/share/${P}/NickvisionMoney.GNOME
	dosym -r /usr/bin/NickvisionMoney.GNOME /usr/bin/denaro
	dosym -r /usr/bin/NickvisionMoney.GNOME /usr/bin/org.nickvision.money

	insinto /usr/share/org.nickvision.money
	doins Resources/org.nickvision.money.gresource

	insinto /usr/share/metainfo
	doins org.nickvision.money.metainfo.xml

	insinto /usr/share/mime/packages
	doins org.nickvision.money.extension.xml

	insinto /usr/share/icons/hicolor/scalable/apps
	doins ../NickvisionMoney.Shared/Resources/org.nickvision.money*.svg

	insinto /usr/share/icons/hicolor/symbolic/apps
	doins Resources/*.svg

	domenu org.nickvision.money.desktop

	dodoc ../CONTRIBUTING.md ../README.md
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_pkg_postinst
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_pkg_postrm
}
