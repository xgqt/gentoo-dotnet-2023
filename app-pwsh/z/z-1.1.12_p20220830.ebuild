# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Quickly navigate the file system based on your cd history"
HOMEPAGE="https://github.com/badmotorfinger/z/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/badmotorfinger/z.git"
else
	if [[ ${PV} == *_p20220830 ]] ; then
		COMMIT=dc2d15c3929ad2007389b828245141d393975b27
		SRC_URI="https://github.com/badmotorfinger/z/archive/${COMMIT}.tar.gz
			-> ${P}.tar.gz"
		S="${WORKDIR}"/${PN}-${COMMIT}
	else
		SRC_URI="https://github.com/badmotorfinger/z/archive/${PV}.tar.gz
			-> ${P}.tar.gz"
	fi
	KEYWORDS="~amd64"
fi

LICENSE="public-domain"
SLOT="${PV%%_p*}"

RDEPEND="virtual/pwsh:*"

PATCHES=( "${FILESDIR}"/z-1.1.12-xdg.patch  )

src_compile() {
	:
}

src_install() {
	insinto /usr/share/GentooPowerShell/Modules/${PN}/${SLOT}
	doins z.*

	einstalldocs
}
