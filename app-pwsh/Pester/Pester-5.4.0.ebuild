# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=6.0
NUGETS="
coverlet.collector-3.1.2
microsoft.aspnetcore.app.runtime.linux-arm-6.0.12
microsoft.aspnetcore.app.runtime.linux-arm64-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-arm-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-arm64-6.0.12
microsoft.aspnetcore.app.runtime.linux-musl-x64-6.0.12
microsoft.aspnetcore.app.runtime.linux-x64-6.0.12
microsoft.codecoverage-17.3.2
microsoft.csharp-4.0.1
microsoft.csharp-4.3.0
microsoft.management.infrastructure-1.0.0
microsoft.net.test.sdk-17.3.2
microsoft.netcore.app.host.linux-arm-6.0.12
microsoft.netcore.app.host.linux-arm64-6.0.12
microsoft.netcore.app.host.linux-musl-arm-6.0.12
microsoft.netcore.app.host.linux-musl-arm64-6.0.12
microsoft.netcore.app.host.linux-musl-x64-6.0.12
microsoft.netcore.app.runtime.linux-arm-6.0.12
microsoft.netcore.app.runtime.linux-arm64-6.0.12
microsoft.netcore.app.runtime.linux-musl-arm-6.0.12
microsoft.netcore.app.runtime.linux-musl-arm64-6.0.12
microsoft.netcore.app.runtime.linux-musl-x64-6.0.12
microsoft.netcore.app.runtime.linux-x64-6.0.12
microsoft.netcore.platforms-1.0.1
microsoft.netcore.platforms-1.1.0
microsoft.netcore.platforms-2.0.0
microsoft.netcore.targets-1.0.1
microsoft.netcore.targets-1.1.0
microsoft.netframework.referenceassemblies.net452-1.0.0
microsoft.netframework.referenceassemblies-1.0.0
microsoft.powershell.3.referenceassemblies-1.0.0
microsoft.powershell.coreclr.eventing-6.0.4
microsoft.powershell.native-6.0.4
microsoft.testplatform.objectmodel-17.3.2
microsoft.testplatform.testhost-17.3.2
microsoft.win32.primitives-4.3.0
microsoft.win32.registry.accesscontrol-4.5.0
microsoft.win32.registry-4.5.0
netstandard.library-1.6.1
netstandard.library-2.0.3
newtonsoft.json-10.0.3
newtonsoft.json-9.0.1
nuget.frameworks-5.11.0
runtime.any.system.collections-4.3.0
runtime.any.system.diagnostics.tools-4.3.0
runtime.any.system.diagnostics.tracing-4.3.0
runtime.any.system.globalization.calendars-4.3.0
runtime.any.system.globalization-4.3.0
runtime.any.system.io-4.3.0
runtime.any.system.reflection.extensions-4.3.0
runtime.any.system.reflection.primitives-4.3.0
runtime.any.system.reflection-4.3.0
runtime.any.system.resources.resourcemanager-4.3.0
runtime.any.system.runtime.handles-4.3.0
runtime.any.system.runtime.interopservices-4.3.0
runtime.any.system.runtime-4.3.0
runtime.any.system.text.encoding.extensions-4.3.0
runtime.any.system.text.encoding-4.3.0
runtime.any.system.threading.tasks-4.3.0
runtime.any.system.threading.timer-4.3.0
runtime.debian.8-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.23-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.fedora.24-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.native.system.io.compression-4.3.0
runtime.native.system.net.http-4.3.0
runtime.native.system.security.cryptography.apple-4.3.0
runtime.native.system.security.cryptography.openssl-4.3.0
runtime.native.system-4.3.0
runtime.opensuse.13.2-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.opensuse.42.1-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.apple-4.3.0
runtime.osx.10.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.rhel.7-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.14.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.04-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.ubuntu.16.10-x64.runtime.native.system.security.cryptography.openssl-4.3.0
runtime.unix.microsoft.win32.primitives-4.3.0
runtime.unix.system.console-4.3.0
runtime.unix.system.diagnostics.debug-4.3.0
runtime.unix.system.io.filesystem-4.3.0
runtime.unix.system.net.primitives-4.3.0
runtime.unix.system.net.sockets-4.3.0
runtime.unix.system.private.uri-4.3.0
runtime.unix.system.runtime.extensions-4.3.0
system.appcontext-4.3.0
system.buffers-4.3.0
system.collections.concurrent-4.3.0
system.collections.nongeneric-4.3.0
system.collections.specialized-4.3.0
system.collections-4.0.11
system.collections-4.3.0
system.componentmodel.primitives-4.3.0
system.componentmodel.typeconverter-4.3.0
system.componentmodel-4.3.0
system.console-4.3.0
system.diagnostics.debug-4.0.11
system.diagnostics.debug-4.3.0
system.diagnostics.diagnosticsource-4.3.0
system.diagnostics.tools-4.0.1
system.diagnostics.tools-4.3.0
system.diagnostics.tracing-4.3.0
system.dynamic.runtime-4.0.11
system.dynamic.runtime-4.3.0
system.globalization.calendars-4.3.0
system.globalization.extensions-4.3.0
system.globalization-4.0.11
system.globalization-4.3.0
system.io.compression.zipfile-4.3.0
system.io.compression-4.3.0
system.io.filesystem.accesscontrol-4.5.0
system.io.filesystem.primitives-4.0.1
system.io.filesystem.primitives-4.3.0
system.io.filesystem-4.0.1
system.io.filesystem-4.3.0
system.io-4.1.0
system.io-4.3.0
system.linq.expressions-4.1.0
system.linq.expressions-4.3.0
system.linq-4.1.0
system.linq-4.3.0
system.management.automation-6.0.4
system.net.http-4.3.0
system.net.nameresolution-4.3.0
system.net.primitives-4.3.0
system.net.sockets-4.3.0
system.objectmodel-4.0.12
system.objectmodel-4.3.0
system.private.datacontractserialization-4.3.0
system.private.uri-4.3.0
system.reflection.emit.ilgeneration-4.0.1
system.reflection.emit.ilgeneration-4.3.0
system.reflection.emit.lightweight-4.0.1
system.reflection.emit.lightweight-4.3.0
system.reflection.emit-4.0.1
system.reflection.emit-4.3.0
system.reflection.extensions-4.0.1
system.reflection.extensions-4.3.0
system.reflection.metadata-1.6.0
system.reflection.primitives-4.0.1
system.reflection.primitives-4.3.0
system.reflection.typeextensions-4.1.0
system.reflection.typeextensions-4.3.0
system.reflection-4.1.0
system.reflection-4.3.0
system.resources.resourcemanager-4.0.1
system.resources.resourcemanager-4.3.0
system.runtime.compilerservices.unsafe-4.5.0
system.runtime.compilerservices.visualc-4.3.0
system.runtime.extensions-4.1.0
system.runtime.extensions-4.3.0
system.runtime.handles-4.0.1
system.runtime.handles-4.3.0
system.runtime.interopservices.runtimeinformation-4.3.0
system.runtime.interopservices-4.1.0
system.runtime.interopservices-4.3.0
system.runtime.numerics-4.3.0
system.runtime.serialization.formatters-4.3.0
system.runtime.serialization.primitives-4.1.1
system.runtime.serialization.primitives-4.3.0
system.runtime.serialization.xml-4.3.0
system.runtime-4.1.0
system.runtime-4.3.0
system.security.accesscontrol-4.5.0
system.security.claims-4.3.0
system.security.cryptography.algorithms-4.3.0
system.security.cryptography.cng-4.3.0
system.security.cryptography.cng-4.5.0
system.security.cryptography.csp-4.3.0
system.security.cryptography.encoding-4.3.0
system.security.cryptography.openssl-4.3.0
system.security.cryptography.pkcs-4.5.0
system.security.cryptography.primitives-4.3.0
system.security.cryptography.x509certificates-4.3.0
system.security.permissions-4.5.0
system.security.principal.windows-4.3.0
system.security.principal.windows-4.5.0
system.security.principal-4.3.0
system.security.securestring-4.3.0
system.text.encoding.codepages-4.5.0
system.text.encoding.extensions-4.0.11
system.text.encoding.extensions-4.3.0
system.text.encoding-4.0.11
system.text.encoding-4.3.0
system.text.regularexpressions-4.1.0
system.text.regularexpressions-4.3.0
system.threading.tasks.extensions-4.0.0
system.threading.tasks.extensions-4.3.0
system.threading.tasks-4.0.11
system.threading.tasks-4.3.0
system.threading.threadpool-4.3.0
system.threading.timer-4.3.0
system.threading-4.0.11
system.threading-4.3.0
system.xml.readerwriter-4.0.11
system.xml.readerwriter-4.3.0
system.xml.xdocument-4.0.11
system.xml.xdocument-4.3.0
system.xml.xmldocument-4.3.0
system.xml.xmlserializer-4.3.0
xunit.abstractions-2.0.3
xunit.analyzers-1.0.0
xunit.assert-2.4.2
xunit.core-2.4.2
xunit.extensibility.core-2.4.2
xunit.extensibility.execution-2.4.2
xunit.runner.visualstudio-2.4.5
xunit-2.4.2
"

inherit check-reqs dotnet-pkg edo

DESCRIPTION="Ubiquitous test and mock framework for PowerShell"
HOMEPAGE="https://pester.dev/
	https://github.com/Pester/Pester/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/${PN}/${PN}.git"
else
	SRC_URI="https://github.com/${PN}/${PN}/archive/${PV}.tar.gz
		-> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

SRC_URI+=" $(nuget_uris) "

LICENSE="MIT"
SLOT="${PV}"

RDEPEND="virtual/pwsh:*"
BDEPEND="${RDEPEND}"

CHECKREQS_DISK_BUILD="2G"
DOTNET_PROJECTS=( "${S}/src/csharp/${PN}/${PN}.csproj" )

DOCS=( BACKERS.md CONTRIBUTING.md README.md SECURITY.md SUPPORT.md docs )

pkg_setup() {
	check-reqs_pkg_setup
	dotnet-pkg_pkg_setup
}

src_unpack() {
	dotnet-pkg_src_unpack

	[[ ${EGIT_REPO_URI} ]] && git-r3_src_unpack
}

src_configure() {
	( cd "${S}"/src/csharp && dotnet-pkg_src_configure )
}

src_compile() {
	dotnet-pkg_src_compile

	edob pwsh ./build.ps1 -Clean:true -Inline:true
}

src_test() {
	dotnet-pkg-utils_test "${S}/src/csharp/PesterTests/PesterTests.csproj"
}

src_install() {
	insinto /usr/share/GentooPowerShell/Modules/${PN}/${PV}
	doins -r bin/.

	einstalldocs
}
