# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=7.0
NUGETS="
microsoft.aspnetcore.app.ref-6.0.14
microsoft.aspnetcore.app.runtime.linux-arm-6.0.14
microsoft.aspnetcore.app.runtime.linux-arm-7.0.3
microsoft.aspnetcore.app.runtime.linux-arm64-6.0.14
microsoft.aspnetcore.app.runtime.linux-arm64-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-arm-6.0.14
microsoft.aspnetcore.app.runtime.linux-musl-arm-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-arm64-6.0.14
microsoft.aspnetcore.app.runtime.linux-musl-arm64-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-x64-6.0.14
microsoft.aspnetcore.app.runtime.linux-musl-x64-7.0.3
microsoft.aspnetcore.app.runtime.linux-x64-6.0.14
microsoft.aspnetcore.app.runtime.linux-x64-7.0.3
microsoft.netcore.app.host.linux-arm-6.0.14
microsoft.netcore.app.host.linux-arm-7.0.3
microsoft.netcore.app.host.linux-arm64-6.0.14
microsoft.netcore.app.host.linux-arm64-7.0.3
microsoft.netcore.app.host.linux-musl-arm-6.0.14
microsoft.netcore.app.host.linux-musl-arm-7.0.3
microsoft.netcore.app.host.linux-musl-arm64-6.0.14
microsoft.netcore.app.host.linux-musl-arm64-7.0.3
microsoft.netcore.app.host.linux-musl-x64-6.0.14
microsoft.netcore.app.host.linux-musl-x64-7.0.3
microsoft.netcore.app.host.linux-x64-6.0.14
microsoft.netcore.app.ref-6.0.14
microsoft.netcore.app.runtime.linux-arm-6.0.14
microsoft.netcore.app.runtime.linux-arm-7.0.3
microsoft.netcore.app.runtime.linux-arm64-6.0.14
microsoft.netcore.app.runtime.linux-arm64-7.0.3
microsoft.netcore.app.runtime.linux-musl-arm-6.0.14
microsoft.netcore.app.runtime.linux-musl-arm-7.0.3
microsoft.netcore.app.runtime.linux-musl-arm64-6.0.14
microsoft.netcore.app.runtime.linux-musl-arm64-7.0.3
microsoft.netcore.app.runtime.linux-musl-x64-6.0.14
microsoft.netcore.app.runtime.linux-musl-x64-7.0.3
microsoft.netcore.app.runtime.linux-x64-6.0.14
microsoft.netcore.app.runtime.linux-x64-7.0.3
microsoft.netcore.platforms-1.1.0
netstandard.library-2.0.3
microsoft.aspnetcore.app.ref-6.0.14
microsoft.aspnetcore.app.runtime.linux-arm-6.0.14
microsoft.aspnetcore.app.runtime.linux-arm-7.0.3
microsoft.aspnetcore.app.runtime.linux-arm64-6.0.14
microsoft.aspnetcore.app.runtime.linux-arm64-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-arm-6.0.14
microsoft.aspnetcore.app.runtime.linux-musl-arm-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-arm64-6.0.14
microsoft.aspnetcore.app.runtime.linux-musl-arm64-7.0.3
microsoft.aspnetcore.app.runtime.linux-musl-x64-6.0.14
microsoft.aspnetcore.app.runtime.linux-musl-x64-7.0.3
microsoft.aspnetcore.app.runtime.linux-x64-6.0.14
microsoft.aspnetcore.app.runtime.linux-x64-7.0.3
microsoft.netcore.app.host.linux-arm-6.0.14
microsoft.netcore.app.host.linux-arm-7.0.3
microsoft.netcore.app.host.linux-arm64-6.0.14
microsoft.netcore.app.host.linux-arm64-7.0.3
microsoft.netcore.app.host.linux-musl-arm-6.0.14
microsoft.netcore.app.host.linux-musl-arm-7.0.3
microsoft.netcore.app.host.linux-musl-arm64-6.0.14
microsoft.netcore.app.host.linux-musl-arm64-7.0.3
microsoft.netcore.app.host.linux-musl-x64-6.0.14
microsoft.netcore.app.host.linux-musl-x64-7.0.3
microsoft.netcore.app.host.linux-x64-6.0.14
microsoft.netcore.app.ref-6.0.14
microsoft.netcore.app.runtime.linux-arm-6.0.14
microsoft.netcore.app.runtime.linux-arm-7.0.3
microsoft.netcore.app.runtime.linux-arm64-6.0.14
microsoft.netcore.app.runtime.linux-arm64-7.0.3
microsoft.netcore.app.runtime.linux-musl-arm-6.0.14
microsoft.netcore.app.runtime.linux-musl-arm-7.0.3
microsoft.netcore.app.runtime.linux-musl-arm64-6.0.14
microsoft.netcore.app.runtime.linux-musl-arm64-7.0.3
microsoft.netcore.app.runtime.linux-musl-x64-6.0.14
microsoft.netcore.app.runtime.linux-musl-x64-7.0.3
microsoft.netcore.app.runtime.linux-x64-6.0.14
microsoft.netcore.app.runtime.linux-x64-7.0.3
microsoft.netcore.platforms-1.1.0
netstandard.library-2.0.3
"

inherit dotnet-pkg

DESCRIPTION="PowerShell framework for REST APIs, Web Sites, and TCP/SMTP servers"
HOMEPAGE="https://badgerati.github.io/Pode/
	https://github.com/Badgerati/Pode/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/Badgerati/${PN}.git"
else
	SRC_URI="https://github.com/Badgerati/${PN}/archive/v${PV}.tar.gz
		-> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

SRC_URI+=" $(nuget_uris) "

LICENSE="MIT"
SLOT="${PV}"
RESTRICT="test"  # Tests fail.

RDEPEND="virtual/pwsh:*"
BDEPEND="${RDEPEND}"

DOTNET_PROJECTS=( "${S}/src/Listener/Pode.csproj" )

src_unpack() {
	dotnet-pkg_src_unpack

	[[ ${EGIT_REPO_URI} ]] && git-r3_src_unpack
}

src_prepare() {
	dotnet-pkg_src_prepare

	sed -i "s|.version.|${PV}|g" src/${PN}*.psd1 || die
}

src_install() {
	rm -r src/Listener || die

	local dest_root=/usr/share/GentooPowerShell/Modules/${PN}/${PV}

	insinto ${dest_root}
	doins -r src/.

	insinto ${dest_root}/Libs/net${DOTNET_COMPAT}
	doins -r ${DOTNET_OUTPUT}/.

	einstalldocs
}
