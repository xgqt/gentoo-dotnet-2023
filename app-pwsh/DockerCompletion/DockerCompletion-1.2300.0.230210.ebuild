# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Docker command completion for PowerShell"
HOMEPAGE="https://github.com/matt9ucci/DockerCompletion/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/matt9ucci/${PN}.git"
else
	if [[ ${PV} == 1.2300.0.230210 ]] ; then
		COMMIT=f03a442d2c78059dce75c2ed3e21713fdaa987c4
		SRC_URI="https://github.com/matt9ucci/${PN}/archive/${COMMIT}.tar.gz
			-> ${P}.tar.gz"
		S="${WORKDIR}"/${PN}-${COMMIT}
	else
		SRC_URI="https://github.com/matt9ucci/${PN}/archive/${PV}.tar.gz
			-> ${P}.tar.gz"
	fi
	KEYWORDS="~amd64"
fi

LICENSE="Apache-2.0 MIT"
SLOT="${PV}"
IUSE="test"
RESTRICT="!test? ( test )"

RDEPEND="
	virtual/pwsh:*
	app-containers/docker
"
BDEPEND="
	test? (
		app-pwsh/Pester
		app-containers/docker
	)
"

PATCHES=( "${FILESDIR}"/${PN}-1.2300.0.230210-tests.patch  )

src_test() {
	ebegin "Running tests for PowerShell module \"${PN}\""
	cd Tests || die
	pwsh -NoProfile -NoLogo -Command \
		"if ((Invoke-Pester -Output Detailed -PassThru).Result -eq 'Failed') \
		{ throw 'tests failed' }"
	eend ${?} || die "tests failed"
}

src_install() {
	insinto /usr/share/GentooPowerShell/Modules/${PN}/${PV}
	doins -r DockerCompletion/.

	einstalldocs
}
