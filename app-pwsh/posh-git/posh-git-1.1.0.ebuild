# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A PowerShell environment for Git"
HOMEPAGE="http://dahlbyk.github.io/posh-git/
	https://github.com/dahlbyk/posh-git/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/dahlbyk/${PN}.git"
else
	SRC_URI="https://github.com/dahlbyk/${PN}/archive/v${PV}.tar.gz
		-> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="${PV}"
IUSE="test"
RESTRICT="!test? ( test )"

RDEPEND="
	virtual/pwsh:*
	dev-vcs/git
"
BDEPEND="
	test? (
		app-pwsh/Pester
		dev-vcs/git
	)
"

DOCS=( CHANGELOG.md ISSUE_TEMPLATE.md README.md profile.example.ps1 )

src_prepare() {
	# Remove bad tests.
	local -a bad_tests=(
		DefaultPrompt.Tests.ps1
		Get-GitBranch.Tests.ps1
		Get-GitDirectory.Tests.ps1
		Get-GitStatus.Tests.ps1
		GitParamTabExpansionVsts.Tests.ps1
		GitPrompt.Tests.ps1
		TabExpansion.Tests.ps1
		Utils.Tests.ps1
	)
	local bad_test
	for bad_test in ${bad_tests[@]} ; do
		rm "${S}"/test/${bad_test} || die
	done

	default
}

src_test() {
	ebegin "Running tests for PowerShell module \"${PN}\""
	cd test || die
	pwsh -NoProfile -NoLogo -Command \
		"if ((Invoke-Pester -Output Detailed -PassThru).Result -eq 'Failed') \
		{ throw 'tests failed' }"
	eend ${?} || die "tests failed"
}

src_install() {
	insinto /usr/share/GentooPowerShell/Modules/${PN}/${PV}
	doins -r src/.

	einstalldocs
}
