# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=7.0
NUGETS="
autofac-6.4.0
basic.reference.assemblies.net60-1.4.1
basic.reference.assemblies.net70-1.4.1
microsoft.build.tasks.git-1.1.1
microsoft.codeanalysis.analyzers-3.0.0
microsoft.codeanalysis.analyzers-3.3.3
microsoft.codeanalysis.common-3.9.0
microsoft.codeanalysis.common-4.4.0-4.final
microsoft.codeanalysis.csharp.scripting-4.4.0-4.final
microsoft.codeanalysis.csharp-4.4.0-4.final
microsoft.codeanalysis.scripting.common-4.4.0-4.final
microsoft.csharp-4.7.0
microsoft.netcore.platforms-2.1.2
microsoft.netcore.platforms-5.0.0
microsoft.netcore.platforms-7.0.0
microsoft.sourcelink.common-1.1.1
microsoft.sourcelink.github-1.1.1
microsoft.win32.registry-5.0.0
newtonsoft.json-13.0.1
nuget.common-6.3.1
nuget.configuration-6.3.1
nuget.frameworks-6.3.1
nuget.packaging-6.3.1
nuget.protocol-6.3.1
nuget.resolver-6.3.1
nuget.versioning-6.3.1
spectre.console.cli-0.45.0
spectre.console-0.45.0
stylecop.analyzers-1.1.118
system.collections.immutable-7.0.0
system.diagnostics.diagnosticsource-4.7.1
system.formats.asn1-5.0.0
system.memory-4.5.4
system.memory-4.5.5
system.reflection.metadata-7.0.0
system.runtime.compilerservices.unsafe-5.0.0
system.runtime.compilerservices.unsafe-6.0.0
system.security.accesscontrol-5.0.0
system.security.cryptography.cng-5.0.0
system.security.cryptography.pkcs-5.0.0
system.security.cryptography.protecteddata-4.4.0
system.security.principal.windows-5.0.0
system.text.encoding.codepages-4.5.1
system.text.encoding.codepages-6.0.0
system.threading.tasks.extensions-4.5.4
"

inherit dotnet-pkg

DESCRIPTION="Cake (C# Make) is a cross platform build automation system"
HOMEPAGE="https://cakebuild.net/
	https://github.com/cake-build/cake/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/cake-build/${PN}.git"
else
	SRC_URI="https://github.com/cake-build/${PN}/archive/v${PV}.tar.gz
		-> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

SRC_URI+=" $(nuget_uris) "

S="${S}"/src/Cake  # We only build "Cake.csproj".

LICENSE="MIT"
SLOT="0"

DOTNET_PROJECTS=( "${S}/Cake.csproj" )

src_unpack() {
	dotnet-pkg_src_unpack

	[[ ${EGIT_REPO_URI} ]] && git-r3_src_unpack
}

src_install() {
	dotnet-pkg_src_install

	dodoc ../../*.md
}
