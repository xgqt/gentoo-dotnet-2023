# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DOTNET_COMPAT=7.0
NUGET_PACKAGES="${WORKDIR}"/nuget_packages

inherit check-reqs dotnet-pkg

DESCRIPTION="The AKEless Build System for .NET"
HOMEPAGE="https://github.com/nuke-build/nuke/"
SRC_URI="
	https://github.com/nuke-build/nuke/archive/${PV}.tar.gz
		-> ${P}.tar.gz
	https://dev.gentoo.org/~xgqt/distfiles/deps/${P}-prebuilt.tar.xz
	https://dev.gentoo.org/~xgqt/distfiles/deps/external-spectreconsole-0.43.0.tar.xz
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="test"  # Fails.

CHECKREQS_DISK_BUILD="6G"
DOTNET_PROJECTS=( "${S}/source/Nuke.GlobalTool/Nuke.GlobalTool.csproj" )

DOCS=( CHANGELOG.md CODE_OF_CONDUCT.md CONTRIBUTING.md README.md )

pkg_setup() {
	check-reqs_pkg_setup
	dotnet-pkg_pkg_setup
}

src_unpack() {
	dotnet-pkg_src_unpack

	[[ ${EGIT_REPO_URI} ]] && git-r3_src_unpack
}

src_prepare() {
	cp -r "${WORKDIR}"/external "${S}" || die

	dotnet-pkg_src_prepare
}

src_install() {
	dotnet-pkg-utils_install
	dotnet-pkg-utils_dolauncher /usr/share/${P}/Nuke.GlobalTool nuke

	einstalldocs
}

pkg_postinst() {
	einfo "Nuke tool executable is installed under the name \"nuke-tool\"."
}
