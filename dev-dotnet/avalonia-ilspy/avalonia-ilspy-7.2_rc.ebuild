# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# NOTICE: Because of custom nuget sources the nugets have to be pre-packaged,
# there is a issue with one of APIs used for this project.

EAPI=8

MY_PN=AvaloniaILSpy
MY_PV=${PV/_/-}
MY_P=${MY_PN}-${MY_PV}

DOTNET_COMPAT=6.0
NUGET_PACKAGES="${WORKDIR}"/nuget_packages

inherit check-reqs desktop dotnet-pkg xdg

DESCRIPTION="Avalonia-based .NET Decompiler, port of ILSpy"
HOMEPAGE="https://github.com/icsharpcode/AvaloniaILSpy/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/icsharpcode/${MY_PN}.git"
else
	SRC_URI="https://github.com/icsharpcode/${MY_PN}/archive/v${MY_PV}.tar.gz
		-> ${P}.tar.gz"
	S="${WORKDIR}"/${MY_P}
	KEYWORDS="~amd64"
fi

SRC_URI+="
	https://dev.gentoo.org/~xgqt/distfiles/deps/${PN}-7.2_rc-prebuilt.tar.xz
"

LICENSE="MIT"
SLOT="0"

RDEPEND="
	app-arch/brotli
	dev-libs/elfutils
	dev-libs/expat
	dev-libs/libxml2
	media-gfx/graphite2
	media-libs/fontconfig
	media-libs/freetype
	media-libs/harfbuzz
	media-libs/libglvnd
	media-libs/libpng
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libX11
	x11-libs/libXau
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXi
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libdrm
	x11-libs/libxcb
	x11-libs/libxshmfence
"

CHECKREQS_DISK_BUILD="3G"
DOTNET_PROJECTS=( "${S}/ILSpy/ILSpy.csproj" )

DOCS=( README.md doc )

pkg_setup() {
	check-reqs_pkg_setup
	dotnet-pkg_pkg_setup
}

src_unpack() {
	dotnet-pkg_src_unpack

	[[ ${EGIT_REPO_URI} ]] && git-r3_src_unpack
}

src_install() {
	dotnet-pkg-utils_install
	dotnet-pkg-utils_dolauncher /usr/share/${P}/ILSpy ${PN}

	doicon --size 48 ILSpy.Core/Images/ILSpy.png
	make_desktop_entry ${PN} ${MY_PN} ILSpy "Development;"

	einstalldocs
}
