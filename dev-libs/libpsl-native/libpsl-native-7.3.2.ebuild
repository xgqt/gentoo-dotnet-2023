# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_PN=PowerShell-Native

inherit cmake

DESCRIPTION="Functionality missing from .NET Core via system calls"
HOMEPAGE="https://github.com/PowerShell/PowerShell-Native/"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/PowerShell/${MY_PN}.git"
else
	SRC_URI="https://github.com/PowerShell/${MY_PN}/archive/v${PV}.tar.gz
		-> ${P}.tar.gz"
	S="${WORKDIR}"/${MY_PN}-${PV}
	KEYWORDS="~amd64 ~x86"
fi

S="${S}"/src/${PN}

LICENSE="MIT"
SLOT="0"
IUSE="test"
RESTRICT="!test? ( test )"

BDEPEND="test? ( dev-cpp/gtest )"

PATCHES=(
	"${FILESDIR}"/${PN}-7.3.2-cmake.patch
	"${FILESDIR}"/${PN}-7.3.2-test-cmake.patch
)

src_configure() {
	local -a mycmakeargs=(
		-DTESTING=$(usex test)
	)
	cmake_src_configure
}

src_install() {
	dolib.so "${BUILD_DIR}"/src/${PN}.so

	einstalldocs
}
